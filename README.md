## How to Install

1. Clone this Repo 

2. Custom DB at .env File
    ```
     DB_CONNECTION=mysql                   
     DB_HOST=127.0.0.1             
     DB_PORT=3306              
     DB_DATABASE=mini-crm             
     DB_USERNAME=your_username           
     DB_PASSWORD=your_password 

    ```  

3. Setup Mailtrap at [Mailtrap.io][identifier]

[identifier]: https://mailtrap.io/ "https://mailtrap.io/signin"

4. Custom mailtrap at .env File
    ```
     MAIL_MAILER=smtp                     
     MAIL_HOST=smtp.mailtrap.io           
     MAIL_PORT=2525                       
     MAIL_USERNAME=your_mailtrap_username 
     MAIL_PASSWORD=your_mailtrap_password 
     MAIL_ENCRYPTION=tls                  
     MAIL_FROM_ADDRESS=                   
     MAIL_FROM_NAME="${APP_NAME}"   

    ```      
 
5. ``` composer install ```

6. Seed the db   
    ``` php artisan migrate:refresh --seed ``` 

7. ``` php artisan storage:link ``` 

8. ``` php artisan serve ``` 

9. To dispatch email to mailtraip after create new company from UI   
    ``` php artisan queue:work ``` 
