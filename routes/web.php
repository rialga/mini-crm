<?php

use App\Dictionary;
use App\Imports\CompanyImport;
use App\Imports\EmployeeImport;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'guest'], function () {
    Route::get('/', function () {
        return view('welcome');
    });
});

Auth::routes(['register' => false]);

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'JwtController@login');
});



Route::group(['middleware' => 'jwt.verify'], function () {

    Route::group(['prefix' => 'company'], function () {
        Route::get('/', 'CompanyController@index');
        Route::get('/search', 'CompanyController@search');
        Route::get('/data', 'CompanyController@data');
        Route::post('/store', 'CompanyController@store');
        Route::get('/show/{id}', 'CompanyController@show');
        Route::post('/update/{id}', 'CompanyController@update');
        Route::delete('/destroy/{id}', 'CompanyController@destroy');
    });

    Route::group(['prefix' => 'employee'], function () {
        Route::get('/', 'EmployeeController@index');
        Route::get('/search', 'EmployeeController@search');
        Route::get('/data', 'EmployeeController@data');
        Route::get('/companyList', 'EmployeeController@companyList');
        Route::post('/store', 'EmployeeController@store');
        Route::get('/show/{id}', 'EmployeeController@show');
        Route::post('/update/{id}', 'EmployeeController@update');
        Route::delete('/destroy/{id}', 'EmployeeController@destroy');
    });

    Route::group(['prefix' => 'item'], function () {
        Route::get('/', 'ItemController@index');
        Route::get('/search', 'ItemController@search');
        Route::get('/data', 'ItemController@data');
        Route::get('/ajax/{id}', 'ItemController@ajax');
        Route::post('/store', 'ItemController@store');
        Route::get('/show/{id}', 'ItemController@show');
        Route::post('/update/{id}', 'ItemController@update');
        Route::delete('/destroy/{id}', 'ItemController@destroy');
    });
    Route::group(['prefix' => 'sell'], function () {
        Route::get('/', 'SellController@index');
        Route::get('/search', 'SellController@search');
        Route::get('/item-list', 'SellController@itemList');
        Route::get('/employee-list', 'SellController@employeeList');
        Route::get('/sell', 'SellController@search');
        Route::get('/data', 'SellController@data');
        Route::post('/store', 'SellController@store');
        Route::get('/show/{id}', 'SellController@show');
        Route::post('/update/{id}', 'SellController@update');
        Route::delete('/destroy/{id}', 'SellController@destroy');
    });


    Route::group(['prefix' => 'sell-summary'], function () {
        Route::get('/', 'SellSummaryController@index');
        Route::get('/search', 'SellSummaryController@search');
        Route::get('/detail/{id}', 'SellSummaryController@detail');

    });
});
