<?php

namespace Tests\Feature;

use App\Company;
use App\Employee;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Session;
use Tests\TestCase;


class UserTest extends TestCase
{
    use RefreshDatabase;

    use DatabaseMigrations;
    /** @test */
    public function onlyLoggedCanDetallFeature()
    {
        $this->get('/company')->assertRedirect('/');
        $this->get('/employee')->assertRedirect('/');
    }

    // Company
    /** @test */
    public function storeCompany()
    {

        $this->withoutExceptionHandling();
        $response = $this->jwtSession();
        $response = $this->post('/company/store', array_merge($this->dataCompany()));
        $response->assertSuccessful();
    }

    /** @test */
    public function validateCompanyName()
    {


        $response = $this->jwtSession();
        $response = $this->post('/company/store', array_merge($this->dataCompany(), ['name' => '']));
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function validateCompanyImageDimension()
    {

        $response = $this->jwtSession();
        $response = $this->post('/company/store', array_merge($this->dataCompany(), ['logo' => UploadedFile::fake()->image('avatar.jpg', 10, 100)->size(100)]));
        $response->assertSessionHasErrors('logo');
    }

    /** @test */
    public function updateCompany()
    {
        $this->withoutExceptionHandling();
        $response = $this->jwtSession();
        $this->post('/company/store', array_merge($this->dataCompany()));

        $modalCompany = Company::first();
        $response = $this->post('company/update/' . $modalCompany->id, array_merge($this->dataCompany(), ['email' => 'edit@edit.com']));
        $response->assertSuccessful();
    }

    /** @test */
    public function deleteCompany()
    {
        $this->withoutExceptionHandling();
        $response = $this->jwtSession();
        $this->post('/company/store', array_merge($this->dataCompany()));

        $modalCompany = Company::first();
        $response = $this->delete('company/destroy/' . $modalCompany->id);
        $response->assertSuccessful();
    }

    // Employee

    /** @test */
    public function storeEmployee()
    {
        $this->withoutExceptionHandling();
        $response = $this->jwtSession();
        $this->post('/company/store', array_merge($this->dataCompany()));
        $modelCompany = Company::first();
        $response = $this->post('/employee/store', array_merge($this->dataEmployee(), ['company' => $modelCompany->id]));
        $response->assertSuccessful();
    }

    /** @test */
    public function updateEmployee()
    {
        $this->withoutExceptionHandling();
        $response = $this->jwtSession();
        $this->post('/company/store', array_merge($this->dataCompany()));
        $modelCompany = Company::first();
        $this->post('/employee/store', array_merge($this->dataEmployee(), ['company' => $modelCompany->id]));
        $modelEmployee = Employee::first();

        $response = $this->post('employee/update/' . $modelEmployee->id, array_merge($this->dataEmployee(), ['email' => 'edit@edit.com']));

        $modelEmployee->refresh();

        $response->assertSuccessful();
    }

    /** @test */
    public function deleteEmployee()
    {
        $this->withoutExceptionHandling();
        $response = $this->jwtSession();
        $this->post('/company/store', array_merge($this->dataCompany()));
        $modelCompany = Company::first();
        $this->post('/employee/store', array_merge($this->dataEmployee(), ['company' => $modelCompany->id]));
        $modelEmployee = Employee::first();

        $response = $this->delete('employee/destroy/' . $modelEmployee->id);

        $response->assertSuccessful();
    }

    /** @test */
    public function validateEmpployeeFirstName()
    {

        $response = $this->jwtSession();
        $response = $this->post('/employee/store', array_merge($this->dataEmployee(), ['firstName' => '']));
        $response->assertSessionHasErrors('firstName');
    }



    /** @test */
    public function validateEmpployeeLastName()
    {
        $response = $this->jwtSession();
        $response = $this->post('/employee/store', array_merge($this->dataEmployee(), ['lastName' => '']));
        $response->assertSessionHasErrors('lastName');
    }

    /** @test */
    public function getDataApiWithToken()
    {

        $response = $this->jwtSession();
        $token = Session::get('jwt_token');


        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token['access_token']
        ])->json('GET', '/api/employee/data/1');
        $response->assertStatus(200);
    }


    private function dataCompany()
    {
        return [
            'name' => 'Company Test',
            'email' => 'company@test.com',
            'website' => 'www.test-web.com',
            'logo' => UploadedFile::fake()->image('avatar.jpg', 100, 100)->size(100),
        ];
    }

    private function dataEmployee()
    {
        return [
            'firstName' => 'test',
            'lastName' => 'testlast',
            'email' => 'employee@test.com',
            'phone' => '09183919',
            'company' => 1
        ];
    }

    private function jwtSession()
    {
        $credetianls = factory(User::class)->create();
        $token_session = $this->actingAs($credetianls)->withSession(['jwt_token' => $credetianls]);
        return $token_session;
    }
}
