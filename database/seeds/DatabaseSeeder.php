<?php

use App\Imports\CompanyImport;
use App\Imports\EmployeeImport;
use App\SellSummary;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Config::set('app.timezone', 'UTC');

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
        ]);


        $data = [
            "Welcome" => "Selamat Datang",
            "Choose" => "Pilih",
            "Normal Price" => "Harga Normal",
            "Add" => "Tambahkan",
            "Sign In" => "Masuk",
            "Price" => "Harga",
            "Item" => "Barang",
            "Discount" => "Korting",
            "Sell" => "Penjualan",
            "Sell Summary" => "Rangkuman Penjualan",
            "Type name , email, company, datetime here" => "Ketik nama, perushaan, email , tangga / waktu disini",
            "Dashboard" =>"Dasbor",
            "Contains" => "Mengandung",
            "Company" => "Perusahaan",
            "Employee" => "Pegawai",
            "Version" => "Versi",
            "Application" => "Aplikasi",
            "All rights reserved" => "Seluruh Hak Cipta",
            "Logout" => "Keluar",
            "Sign in to get more feature" => "Masuk Untuk Mendapatkan Fitur Lebih",
            "Type Name and Price here" => "Ketik Nama dan Harga disini",
            "Lang" => "Bahs",
            "Companies data" => "Data Perusahaan",
            "Add Company" => "Tambahkan Perusahaan",
            "Name" => "Nama",
            "Website" => "Situs Web",
            "Action" => "Aksi",
            "Enter" => "Masukkan",
            "Enter Name" => "Masukkan Nama",
            "Enter Email" => "Masukkan Email",
            "Enter Phone" => "Masukkan Nomer Telepon",
            "Enter Website" => "Masukkan Situs Web",
            "Enter First Name" => "Masukkan Nama Depan",
            "Enter Last Name" => "Masukkan Nama Belakang",
            "Minimum Dimension" => "Dimensi Minimum",
            "Choose File" => "Pilih Berkas",
            "Change Logo" => "Ganti Logo",
            "Change Password" => "Ganti Kata Sandi",
            "Update" => "Perbarui",
            "Create" => "Buat",
            "Submit" => "Kirimkan",
            "First Name" => "Nama Depan",
            "Close" => "Tutup",
            "Select Option" => "Pilih Opsi",
            "Phone" => "Telepon",
            "Last Name" => "Nama Belakang",
            "Do you want to Delete?" => "Anda Yakin untuk Menghapus?",
            "Choose Company" => "Pilih Perusahaan",
            "Company Detail" => "Detail Perusahaan",
            "Add Employee" => "Tambahkan Pegawai",
            "Employees data" => "Data Pegawai",
            "Cancel" => "Batal",
            "Sending Email Error" => " Pengiriman Email Bermasalah ",
            "Create Successfully" => "Berhasil Menambahkan data",
            "Create Unsuccessfull" => "Gagal Menambahkan data",
            "Update Successfully" => "Berhasil Mengubah data",
            "Update Unsuccessfull" => "Gagal Mrgnubah data",
            "Delete Unsuccessfull" => "Hapus Data Gagal",
            "Delete Successfully" => "Hapus Data Berhasil",
            "invalid email or password" => "Email ata Password Salah",
            "Email cannot be empty" => "Email Tidak Boleh Kosong",
            "Join Date" => "Tanggal Bergabung",
            "Password cannot be empty" => "Password Tidak Boleh Kosong",
            "Date" => "Tanggal",
            "Time" => "Waktu",
            "Time Zone" => "Zona Waktu",
            "created by" => "dibuat oleh",
            "updated by" => "diubah oleh",
            "Password" => "Kata Sandi",
            "Enter Password" => "Masukkan Kata Sandi"
        ];

        foreach ($data as $key => $val) {
            DB::table('dictionaries')->insert([
                'key' => $key,
                'id' => $val
            ]);
        }


        Excel::import(new CompanyImport, 'dummy_company.xlsx');

        Excel::import(new EmployeeImport, 'dummy_employee.xlsx');

        $item = ['1'=>'Phone', '2'=>'Tablet', '3'=>'Smart TV', '4'=>'Processor', '5'=>'Home Assistan'];
        $price = ['1'=>'2000000' , '2'=>'3500000' , '3'=>'5000000', '4'=>'2500000', '5'=>'8000000'];
        foreach($item as $num => $value){
            DB::table('items')->insert([
                'name' => $value,
                'price'=> $price[$num],
            ]);
        }


        for($i = 1; $i <= 100; $i++){


            $disc = rand(1,50);
            $employee_id = rand(1,50);

            $item_id = rand(1,5);
            $created_at = Carbon::now()->subHours(rand(1, 55));

            $price_norm = $price[$item_id];
            $potongan = $price_norm * ($disc/100);
            $after_discount = $price_norm - $potongan;

            DB::table('sells')->insert([
                'price' => $price_norm,
                'discount'=> $disc,
                'item_id'=> $item_id,
                'employee_id'=> $employee_id,
                'created_at'=> $created_at,
            ]);


            $summary = SellSummary::where('employee_id',$employee_id)
                        ->whereDate('created_at' , $created_at)->first();
            if($summary){
                $summary->price_total += $price_norm;
                $summary->discount_total += $potongan;
                $summary->total += $after_discount;
                $summary->update();
            }
            else{
                DB::table('sell_summaries')->insert([
                    'price_total' => $price_norm,
                    'discount_total'=> $potongan,
                    'total' => $after_discount,
                    'employee_id'=> $employee_id,
                    'created_at'=> $created_at,
                ]);
            }

        }

    }
}
