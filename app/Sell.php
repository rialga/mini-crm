<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Throwable;

class Sell extends Model
{
    protected $table = 'sells';
    protected $primaryKey = 'id';
    protected $fillable = [
        'price' , 'discount' , 'item_id' , 'employee_id'
    ];


    public function item() {
        return $this->belongsTo('App\Item', 'item_id', 'id');
    }
    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id', 'id');
    }

    public function scopeSearch($query,$val){

        try {
            $time = Carbon::createFromFormat('H:i:s', $val, Session::get('timezone'))->setTimezone('UTC');
            $val = $time->toTimeString();
        } catch (Throwable $e) {}


        try {
            $time = Carbon::createFromFormat('Y-m-d H:i:s', $val, Session::get('timezone'))->setTimezone('UTC');
            $val =  $time->format('Y-m-d H:i:s');
        } catch (Throwable $e) {}


        return $query
        ->where('discount','like','%' .$val. '%')
        ->orWhereHas('employee', function ($query) use ($val) {
            $query->where('first_name', 'like', '%' . $val . '%');
            $query->Orwhere('last_name', 'like', '%' . $val . '%');
        })
        ->orWhereHas('item', function ($query) use ($val) {
            $query->where('name', 'like', '%' . $val . '%');
        })
        ->Orwhere('price','like','%' .$val. '%')
        ->Orwhere('created_at', $val)
        ->OrwhereDate('created_at', $val)
        ->OrwhereTime('created_at', $val)
        ;
    }
}
