<?php

namespace App\Imports;

use App\Employee;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EmployeeImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {

        return new Employee([
            'first_name'=> $row['first_name'],
            'last_name'=> $row['last_name'],
            'companies_id'=> $row['companies_id'],
            'email'=> $row['email'] ,
            'phone'=> $row['phone'],
        ]);
    }


}
