<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Throwable;

class Company extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name', 'email','logo', 'website' ,'created_by_id' , 'updated_by_id'
    ];

    public function company() {
        return $this->hasMany('App\Employee', 'companies_id', 'id');
    }

    public function createdBy() {
        return $this->belongsTo('App\User', 'created_by_id', 'id');
    }
    public function updatedBy() {
        return $this->belongsTo('App\user', 'updated_by_id', 'id');
    }


    public function scopeSearch($query,$val){

        try {
            $time = Carbon::createFromFormat('H:i:s', $val, Session::get('timezone'))->setTimezone('UTC');
            $val = $time->toTimeString();
        } catch (Throwable $e) {}


        try {
            $time = Carbon::createFromFormat('Y-m-d H:i:s', $val, Session::get('timezone'))->setTimezone('UTC');
            $val =  $time->format('Y-m-d H:i:s');
        } catch (Throwable $e) {}

        return $query
        ->where('name','like','%' .$val. '%')
        ->Orwhere('email','like','%' .$val. '%')
        ->Orwhere('website','like','%' .$val. '%')
        ->OrwhereTime('created_at' , $val)
        ->Orwhere('created_at', $val)
        ->OrwhereDate('created_at', $val);

    }
}
