<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Throwable;

class Employee extends Model
{
    protected $table = 'employees';
    protected $primaryKey = 'id';
    protected $fillable = [
        'first_name', 'last_name', 'companies_id', 'email', 'phone', 'password', 'created_by_id', 'updated_by_id'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company', 'companies_id', 'id');
    }
    public function createdBy()
    {
        return $this->belongsTo('App\User', 'created_by_id', 'id');
    }
    public function updatedBy()
    {
        return $this->belongsTo('App\User', 'updated_by_id', 'id');
    }


    public function sell() {
        return $this->hasMany('App\Sell', 'employee_id', 'id');
    }

    public function scopeSearch($query, $val)
    {
        try {
            $time = Carbon::createFromFormat('H:i:s', $val, Session::get('timezone'))->setTimezone('UTC');
            $val = $time->toTimeString();
        } catch (Throwable $e) {}

        try {
            $time = Carbon::createFromFormat('Y-m-d H:i:s', $val, Session::get('timezone'))->setTimezone('UTC');
            $val =  $time->format('Y-m-d H:i:s');
        } catch (Throwable $e) {}


        return $query
            ->where('first_name', 'like', '%' . $val . '%')
            ->Orwhere('last_name', 'like', '%' . $val . '%')
            ->Orwhere('email', 'like', '%' . $val . '%')
            ->Orwhere('phone', 'like', '%' . $val . '%')
            ->Orwhere('created_at', $val)
            ->OrwhereDate('created_at', $val)
            ->orWhereHas('company', function ($query) use ($val) {
                $query->where('name', 'like', '%' . $val . '%');
            })
            ->OrwhereTime('created_at', $val);
    }
}
