<?php

namespace App\Http\Controllers;

use App\Sell;
use App\SellSummary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SellSummaryController extends Controller
{
    public function index(Request $request){

        $request->pagedd == null ? $pagedd= 5 : $pagedd= $request->pagedd;

        $date=(Carbon::today()->setTimezone('UTC'));

        $date_range = $date->format('m/d/Y').' - '.$date->format('m/d/Y');

        // $data = SellSummary::whereDate('created_at' , $date)
        // ->orderBy('created_at','DESC')
        // ->paginate($pagedd);

        $data = SellSummary::orderBy('created_at','DESC')
        ->paginate($pagedd);

        return view('pages.sell_summary.sell-summary' , compact('data' , 'pagedd' , 'date_range'));
    }

    public function detail(Request $request, $id){

        $request->pagedd == null ? $pagedd= 5 : $pagedd= $request->pagedd;
        $sell_summary = SellSummary::where('id' , $id)->first();

        $sell = Sell::where('employee_id' , $sell_summary->employee_id)
        ->whereDate('created_at' , $sell_summary->created_at)
        ->orderBy('created_at','DESC')
        ->paginate($pagedd);
        return view('pages.sell_summary.detail' , compact('sell' , 'pagedd' , 'id' , 'sell_summary'));
    }

    public function search(Request $request){

        $request->pagedd == null ? $pagedd= 5 : $pagedd= $request->pagedd;

        $date = explode('-',trim(str_replace(' ', '', $request->date_range)));

        $search = $request->search;
        $date_range = $date[0].' - '.$date[1];

        if($date[0] == $date[1]){

            $data = SellSummary::search($search)
            ->whereDate('created_at', [Carbon::parse($date[0])])
            ->orderBy('created_at','DESC')
            ->with(['employee'])
            ->paginate($pagedd);
        }
        else{
            $data = SellSummary::search($search)
            ->whereBetween('created_at', [Carbon::parse($date[0]),Carbon::parse($date[1])->addDay()])
            ->orderBy('created_at','DESC')
            ->with(['employee'])
            ->paginate($pagedd);
        }

        return view('pages.sell_summary.sell-summary-search' , compact('data' , 'pagedd' , 'date_range' , 'search'));
    }
}
