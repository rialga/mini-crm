<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ItemController extends Controller
{
    public function index(){
        return view('pages.item.item');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $pagedd= $request->pagedd;

        $item_data=  Item::search($search)
        ->paginate($pagedd);

        return view('pages.item.item-search' ,compact('item_data' , 'search' ,'pagedd'));
    }
    public function data(){

        $item_data = Item::all();

        return DataTables::of($item_data)->toJson();
    }

    public function store(Request $request){

        $validated = $request->validate([
            'name' => 'required|max:99',
            'price' => 'required',

        ]);

        $item = new Item();

        $item->name = $request->name;
        $item->price = $request->price;

        if ($item->save()) {
            return ['status' => true, 'message' => __('text.Create Successfully')];
        } else {
            return ['status' => false, 'message' => __('text.Create Unsuccessfull')];
        }

    }

    public function update(Request $request , $id){

        $validated = $request->validate([
            'name' => 'required|max:99',
            'price' => 'required',
        ]);

        $item = Item::where('id', $id)->first();

        $item->name = $request->name;
        $item->price = $request->price;

        if ($item->update()) {
            return ['status' => true, 'message' => __('text.Create Successfully')];
        } else {
            return ['status' => false, 'message' => __('text.Create Unsuccessfull')];
        }
    }

    public function destroy($id)
    {

        $data = Item::where('id', $id)->first();

        if ($data->delete()) {
            return ['status' => true, 'message' =>  __('text.Delete Successfully')];
        } else {
            return ['status' => false, 'message' =>  __('text.Delete Unsuccessfull')];
        }
    }

    public function ajax($id){
        $data = Item::where('id', $id)->first();
        return $data;
    }
}
