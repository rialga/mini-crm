<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class ApiDataController extends Controller
{
    public function dataEmployee($id)
    {
        $employee = Employee::where('companies_id' , $id)->with('company', 'updatedBy', 'createdBy')->get();

        if ($employee->count() > 0) {
            return DataTables::of($employee)->toJson();
        } else {
            return response(['status' => "Employee with company_id = '$id' Not Found"] ,204);
        }
    }
}
