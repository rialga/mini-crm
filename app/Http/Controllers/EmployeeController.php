<?php

namespace App\Http\Controllers;

use App\Company;
use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class EmployeeController extends Controller
{
    public function index()
    {
        return view('pages.employee.employee');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $pagedd= $request->pagedd;


        $employee_data=  Employee::search($search)
        ->with(['company'])
        ->paginate($pagedd);
        return view('pages.employee.employee-search' ,compact('employee_data' , 'search' ,'pagedd'));
    }

    public function store(Request $request)
    {
        $timezone = Session::get('timezone');

        $validated = $request->validate([
            'firstName' => 'required|max:15',
            'lastName' => 'required|max:15',
            'email' => 'unique:employees|max:45|nullable',
            'phone' => 'unique:employees|max:15|nullable',
        ]);


        $employee = new Employee;

        $employee->first_name = $request->firstName;
        $employee->last_name = $request->lastName;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->companies_id = $request->company;
        $employee->password = Hash::make($request->password);
        $employee->created_by_id = Auth::user()->id;


        Config::set('app.timezone', 'UTC');
        if ($employee->save()) {
            Config::set('app.timezone', $timezone);
            return ['status' => true, 'message' =>  __('Create Successfully')];
        } else {
            return ['status' => false, 'message' =>  __('Create Unsuccessfull')];
        }
    }

    public function update(Request $request, $id)
    {
        $timezone = Session::get('timezone');
        $employee = Employee::where('id', $id)->first();

        $validated = $request->validate([
            'firstName' => 'required|max:15',
            'lastName' => 'required|max:15',
            'email' => 'nullable|max:45|unique:employees,email,' . $employee->id,
            'phone' => 'nullable|max:15|unique:employees,phone,' . $employee->id,
        ]);

        if ($request->password) {
            $employee->password = Hash::make($request->password);
        }

        $employee->first_name = $request->firstName;
        $employee->last_name = $request->lastName;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->companies_id = $request->company;
        $employee->updated_by_id = Auth::user()->id;

        Config::set('app.timezone', 'UTC');

        if ($employee->update()) {
            Config::set('app.timezone', $timezone);
            return ['status' => true, 'message' =>  __('Update Successfully')];
        } else {
            return ['status' => false, 'message' =>  __('Update Unsuccessfull')];
        }
    }

    public function destroy($id)
    {
        $data = Employee::where('id', $id)->first();

        if ($data->delete()) {
            return ['status' => true, 'message' =>  __('Delete Successfully')];
        } else {
            return ['status' => false, 'message' =>  __('Delete Unsuccessfull')];
        }
    }

    public function data()
    {

        $employee = Employee::with('company', 'updatedBy', 'createdBy')->get();

        return DataTables::of($employee)->toJson();
    }

    public function companyList()
    {
        $company  = Company::all();
        return json_encode($company);
    }
}
