<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Item;
use App\Sell;
use App\SellSummary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class SellController extends Controller
{
    public function index()
    {
        return view('pages.sell.sell');
    }

    public function search(Request $request){
        $search = $request->search;
        $pagedd= $request->pagedd;

        $sell_data=  Sell::search($search)
        ->with(['employee' , 'item'])
        ->paginate($pagedd);

        return view('pages.sell.sell-search' ,compact('sell_data' , 'search' ,'pagedd'));
    }

    public function store(Request $request)
    {
        $timezone = Session::get('timezone');
        Config::set('app.timezone', 'UTC');

        $validated = $request->validate([
            'discount' => 'required',
            'employee' => 'required',
            'item' => 'required',

        ]);

        $item = Item::where('id', $request->item)->first();
        $discount = $item->price * ($request->discount / 100);
        $after_discount = $item->price - $discount;

        $sell = new Sell();
        $sell->price  = $item->price;
        $sell->discount = $request->discount;
        $sell->employee_id = $request->employee;
        $sell->item_id = $request->item;

        if ($sell->save()) {
            $sell_summary = SellSummary::where('employee_id', $request->employee)
            ->whereDate('created_at', Carbon::today())->first();
            if ($sell_summary) {
                $sell_summary->price_total += $sell->price;
                $sell_summary->discount_total += $discount;
                $sell_summary->employee_id = $request->employee;
                $sell_summary->total += $after_discount;
                $sell_summary->update();
            } else {
                $sell_summary = new SellSummary();
                $sell_summary->price_total += $sell->price;
                $sell_summary->discount_total += $discount;
                $sell_summary->total += $after_discount;
                $sell_summary->employee_id = $request->employee;
                $sell_summary->save();
            }
            Config::set('app.timezone', $timezone);
            return ['status' => true, 'message' => __('text.Create Successfully')];
        } else {
            return ['status' => false, 'message' => __('text.Create Unsuccessfull')];
        }
    }


    public function update($id , Request $request){
        $timezone = Session::get('timezone');
        Config::set('app.timezone', 'UTC');

        $validated = $request->validate([
            'discount' => 'required',
            'employee' => 'required',
            'item' => 'required',

        ]);

        $item = Item::where('id', $request->item)->first();

        $sell = Sell::where('id', $id)->first();

        $summary = SellSummary::where('employee_id' , $sell->employee_id)->whereDate('created_at' , $sell->created_at)->first();
        $discount = $sell->price * ($sell->discount / 100);
        $after_discount = $sell->price - $discount;

        if($summary){
            $summary->price_total -= $sell->price;
            $summary->discount_total -= $discount;
            $summary->total -= $after_discount;
            $summary->update();
        }

        $summary_update = SellSummary::where('employee_id' , $request->employee)->whereDate('created_at' , $sell->created_at)->first();

        if($summary_update){
            $summary_update->price_total += $item->price;
            $summary_update->discount_total += $item->price * ($request->discount / 100);
            $summary_update->total += $item->price - ($item->price * ($request->discount / 100));
            $summary_update->update();

        }
        else{
            $summary_new = new SellSummary();
            $summary_new->price_total += $item->price;
            $summary_new->discount_total += $item->price * ($request->discount / 100);
            $summary_new->total += $item->price - ($item->price * ($request->discount / 100));
            $summary_new->employee_id = $request->employee;
            $summary_new->created_at = $summary->created_at;
            $summary_new->save();
        }

        $destroy = SellSummary::where('employee_id' , $sell->employee_id)->whereDate('created_at' , $sell->created_at)->first();
        if($destroy->discount_total== 0 && $destroy->price_total == 0 && $destroy->total==0){
            $destroy->delete();
        }

        $sell->price  = $item->price;
        $sell->discount = $request->discount;
        $sell->employee_id = $request->employee;
        $sell->item_id = $request->item;

        if ($sell->update()) {
            Config::set('app.timezone', $timezone);
            return ['status' => true, 'message' => __('text.Update Successfully')];
        } else {
            return ['status' => false, 'message' => __('text.Update Unsuccessfull')];
        }
    }

    public function destroy($id){
        $sell = Sell::where('id' , $id)->first();


        $discount = $sell->price * ($sell->discount / 100);
        $after_discount = $sell->price - $discount;


        $sell_summary = SellSummary::where('employee_id' , $sell->employee_id)
        ->whereDate('created_at' , $sell->created_at)->first();

        if($sell_summary){
            $sell_summary->price_total -= $sell->price;
            $sell_summary->discount_total -= $discount;
            $sell_summary->total -= $after_discount;
            $sell_summary->update();

            $destroy_summary = SellSummary::where('employee_id' , $sell->employee_id)
            ->whereDate('created_at' , $sell->created_at)->first();

            if($destroy_summary->discount_total== 0 && $destroy_summary->price_total == 0 && $destroy_summary->total==0){
                $destroy_summary->delete();
            }

        }

        if ($sell->delete()) {
            return ['status' => true, 'message' =>  __('text.Delete Successfully')];
        } else {
            return ['status' => false, 'message' =>  __('text.Delete Unsuccessfull')];
        }
    }

    public function data()
    {
        $data = Sell::with(['employee', 'item'])->get();
        return DataTables::of($data)->toJson();
    }

    public function employeeList()
    {
        $employee  = Employee::all();
        return json_encode($employee);
    }

    public function itemList()
    {
        $item  = Item::all();
        return json_encode($item);
    }
}
