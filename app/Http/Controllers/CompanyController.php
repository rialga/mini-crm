<?php

namespace App\Http\Controllers;

use App\Company;
use App\Jobs\NotifCompanyJob;
use App\Mail\NewCompanyNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{


    public function index()
    {

        // dd(Session::get('jwt_token'));
        return view('pages.company.company');
    }


    public function search(Request $request)
    {
        $search = $request->search;
        $pagedd= $request->pagedd;

        $company_data=  Company::search($search)
        ->paginate($pagedd);

        return view('pages.company.company-search' ,compact('company_data' , 'search' ,'pagedd'));
    }


    public function data()
    {

        $company = Company::with('createdBy' , 'updatedBy')->get();
        return DataTables::of($company)->toJson();
    }

    public function store(Request $request)
    {

        $timezone = Session::get('timezone');

        $validated = $request->validate([
            'name' => 'required|max:99',
            'email' => 'unique:companies|max:99|nullable',
            'website' => 'unique:companies|max:99|nullable',
            'logo' => 'dimensions:min_width=100,min_height=100'
        ]);


        $company = new Company;

        if ($request->hasFile('logo')) {

            $extension = $request->file('logo')->getClientOriginalExtension();
            $imageName = 'logo-' . $request->name . '-' . rand(0, 99999) . '.' . $extension;

            $request->file('logo')->storeAs('public/uploads', $imageName);
            $company->logo = $imageName;
        }

        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->created_by_id = Auth::user()->id;


        if ($request->email) {

            try {
                $job = (new NotifCompanyJob($request->email))->delay(Carbon::now()->addSeconds(5));
                dispatch($job);
            } catch (\Exception $e) {
                return ['message' => _('Sending Email Error')];
            }
        }
        Config::set('app.timezone', 'UTC');

        if ($company->save()) {
            Config::set('app.timezone', $timezone);
            return ['status' => true, 'message' => __('text.Create Successfully')];
        } else {
            return ['status' => false, 'message' => __('text.Create Unsuccessfull')];
        }
    }

    public function update(Request $request, $id)
    {
        $timezone = Session::get('timezone');

        $company = Company::where('id', $id)->first();


        $validated = $request->validate([
            'name' => 'required|max:99',
            'email' => 'nullable|max:99|unique:companies,email,' . $company->id,
            'website' => 'nullable|max:99|unique:companies,website,' . $company->id,
        ]);

        if ($request->file('logo')) {
            $validated = $request->validate([
                'logo' => 'dimensions:min_width=100,min_height=100'
            ]);
            if ($company->logo != null) {
                Storage::delete('public/uploads/' . $company->logo);
            }

            $extension = $request->file('logo')->getClientOriginalExtension();
            $imageName = 'logo-' . $request->name . '-' . rand(0, 99999) . '.' . $extension;
            $request->file('logo')->storeAs('public/uploads', $imageName);
            $company->logo = $imageName;
        }

        $company->name = $request->name;
        $company->email = $request->email;
        $company->website = $request->website;
        $company->updated_by_id = Auth::user()->id;

        Config::set('app.timezone', 'UTC');

        if ($company->update()) {
            Config::set('app.timezone', $timezone);
            return ['status' => true, 'message' =>  __('text.Update Successfully')];
        } else {
            return ['status' => false, 'message' =>  __('text.Update Unsuccessfull')];
        }
    }

    public function destroy($id)
    {

        $data = Company::where('id', $id)->first();
        if (Storage::exists('public/uploads/' . $data->logo)) {
            Storage::delete('public/uploads/' . $data->logo);
        }


        if ($data->delete()) {
            return ['status' => true, 'message' =>  __('text.Delete Successfully')];
        } else {
            return ['status' => false, 'message' =>  __('text.Delete Unsuccessfull')];
        }
    }
}
