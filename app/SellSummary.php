<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellSummary extends Model
{
    protected $table = 'sell_summaries';
    protected $primaryKey = 'id';
    protected $fillable = [
        'price_total' , 'discount_total' ,  'employee_id' , 'total' ,'created_at'
    ];

    public function employee() {
        return $this->belongsTo('App\Employee', 'employee_id', 'id');
    }

    public function scopeSearch($query,$val){

        return $query
        ->where('price_total','like','%' .$val. '%')
        ->Orwhere('discount_total','like','%' .$val. '%')
        ->Orwhere('total','like','%' .$val. '%')
        ->orWhereHas('employee', function ($query) use ($val) {
            $query->where('first_name', 'like', '%' . $val . '%');
            $query->Orwhere('last_name', 'like', '%' . $val . '%');
            $query->orWhereHas('company' , function($subquery) use ($val){
                $subquery->where('name' , 'like', '%' . $val . '%');
            });
        });
    }
}
