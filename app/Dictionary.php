<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    protected $primaryKey = 'key';
    protected $fillable = [
        'key','id'
    ];

    public $incrementing = false;
    public $timestamps = false;


}
