<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $primaryKey = 'id';
    protected $fillable = [
        'price','name'
    ];

    public $timestamps = false;

    public function scopeSearch($query,$val){

        return $query
        ->where('name','like','%' .$val. '%')
        ->Orwhere('price','like','%' .$val. '%');
    }
}
