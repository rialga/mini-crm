<?php

use App\Dictionary;

$eloquent = Dictionary::all();

$data = [];

foreach($eloquent as $dict){
    $data [$dict->key] = $dict->key;
}

return $data;
