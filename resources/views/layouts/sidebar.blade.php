@section('sidebar')

    <!-- Brand Logo -->
    <a href="#" class="brand-link ml-3">
        <!-- <img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
        <i class="fas fa-adjust fa-fw mr-2"></i>
        <span class="brand-text font-weight-bold">Mini-CRM</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        <!-- Sidebar Menu -->
        <nav class="mt-3">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url('/company') }}" class="nav-link {{ Request::url() == url('/company') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-building"></i>
                        <p> {{ __('text.Company') }} </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/employee') }}" class="nav-link {{ Request::url() == url('/employee') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-users"></i>
                        <p> {{ __('text.Employee') }} </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/item') }}" class="nav-link {{ Request::url() == url('/item') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-cubes"></i>
                        <p> {{ __('text.Item') }} </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/sell') }}" class="nav-link {{ Request::url() == url('/sell') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p> {{ __('text.Sell') }} </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/sell-summary') }}" class="nav-link {{ Request::url() == url('/sell-summary') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-clipboard"></i>
                        <p> {{ __('text.Sell Summary') }} </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>

@endsection


