<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mini-CRM</title>


    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">

    <link rel="icon" type="image/png" href="{{ asset('favicon.ico') }}" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href={{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}>
    <link rel="stylesheet" href={{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}>
    <link rel="stylesheet" href={{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}>

    <!-- SweetAlert2 -->
    <link rel="stylesheet" href={{ asset('assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}>

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.min.css') }}">
</head>



@guest

    <body class="hold-transition login-page">
        <div class="login-box">

            @yield('login')

        </div>

        <script src={{ asset('assets/plugins/jquery/jquery.min.js') }}></script>

        <!-- Bootstrap 4 -->
        <script src={{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}></script>
        <!-- AdminLTE App -->
        <script src={{ asset('assets/dist/js/adminlte.min.js') }}></script>
        <script src={{ asset('assets/jqueryCookies.js') }}></script>


        <!-- SweetAlert2 -->
        <script src={{ asset('assets/plugins/sweetalert2/sweetalert2.min.js') }}></script>

        @yield('js')
    </body>

    {{-- ADMIN TEMPLATE --}}
@else

    <body class="hold-transition sidebar-mini layout-fixed">
        <!-- Site wrapper -->

        <div class="wrapper">
            <!-- Header -->
            <nav class="main-header navbar navbar-expand navbar-white navbar-light">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                    </li>
                </ul>


                <!-- Right navbar links -->
                <ul class="navbar-nav ml-auto">

                    <!-- Notifications Dropdown Menu -->

                    <li class="nav-item">
                        <table>
                            <td style="padding: 5px;"> {{ __('Time Zone') }} : </td>
                            <td>
                                <select class="custom-select" id="timezone" name="company" onchange="if (this.value) window.location.href=this.value">
                                    <option value=""> {{ Session::get('timezone') }} </option>
                                </select>
                            </td>
                        </table>
                    </li>


                    <li class="nav-item">
                        @if (count(config('app.languages')) > 1)
                    <li class="nav-item dropdown d-md-down-none">

                        <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                            aria-expanded="false"> {{ __('text.Lang') }} :
                            {{ strtoupper(app()->getLocale()) }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @foreach (config('app.languages') as $langLocale => $langName)
                                <a class="dropdown-item"
                                    href="{{ url()->current() }}?change_language={{ $langLocale }}">{{ strtoupper($langLocale) }}
                                    ({{ $langName }})</a>
                            @endforeach
                        </div>
                    </li>
                    @endif
                    </li>

                    <li class="nav-item">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                                                                                                                                    document.getElementById('logout-form').submit();">
                                    {{ __('text.Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </li>
                </ul>
            </nav>
            <!-- /.Header -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                @yield('sidebar')
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                @yield('content')
            </div>
            <!-- /.content-wrapper -->

            <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b>{{ __('text.Version') }}</b> x.x.x
                </div>
                <strong>{{ __('text.Application') }}</a>.</strong> {{ __('text.All rights reserved') }}.
            </footer>


            <!-- /.control-sidebar -->
        </div>

        <!-- ./wrapper -->

        <!-- jQuery -->
        <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- overlayScrollbars -->
        <script src="{{ asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('assets/dist/js/adminlte.min.js') }}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{ asset('assets/dist/js/demo.js') }}"></script>
        <script src={{ asset('assets/jqueryCookies.js') }}></script>

        <!-- InputMask -->
        <script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/inputmask/jquery.inputmask.min.js') }}"></script>

        <!-- date-range-picker -->
        <script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>

        <!-- SweetAlert2 -->
        <script src={{ asset('assets/plugins/sweetalert2/sweetalert2.min.js') }}></script>

        <!-- DataTables  & Plugins -->
        <script src={{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}></script>
        <script src={{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}></script>
        <script src={{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}></script>
        <script src={{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}></script>
        <script src={{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}></script>
        <script src={{ asset('assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}></script>
        <script src={{ asset('assets/plugins/jszip/jszip.min.js') }}></script>
        <script src={{ asset('assets/plugins/pdfmake/pdfmake.min.js') }}></script>
        <script src={{ asset('assets/plugins/pdfmake/vfs_fonts.js') }}></script>
        <script src={{ asset('assets/plugins/datatables-buttons/js/buttons.html5.min.js') }}></script>
        <script src={{ asset('assets/plugins/datatables-buttons/js/buttons.print.min.js') }}></script>
        <script src={{ asset('assets/plugins/datatables-buttons/js/buttons.colVis.min.js') }}></script>

        <script>
            $(document).ready(function() {
                var url_param = {!! json_encode(url()->current()) !!} + '?change_timezone='

                $.ajax({
                    url: 'http://worldtimeapi.org/api/timezone',
                    dataType: "json",
                    success: function(data) {
                        var time_data = jQuery.parseJSON(JSON.stringify(data));
                        $.each(time_data, function(k, v) {

                            $('#timezone').append($('<option>', {
                                value: url_param + v
                            }).text(v))
                        })
                    }
                });

                $('#date_range').daterangepicker()
            });


        </script>

        @yield('js')

    </body>
@endguest


</html>
