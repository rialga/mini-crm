@extends('layouts.main')

@section('login')

    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a style="cursor: pointer;" class="h1"><b>mini</b><b style="color: blue">-</b>CRM</a>
        </div>
        <div class="card-body">
            <p class="login-box-msg">{{ __('text.Sign in to get more feature') }}</p>

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="input-group mb-3">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email"
                        name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>

                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>


                <div class="input-group mb-3">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password"
                        required autocomplete="current-password">

                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="row justify-content-end">
                    <div class="col-8">

                        @if (count(config('app.languages')) > 1)

                            <a class="btn btn-info" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
                                aria-expanded="false" style="color: white">
                                {{ __('text.Lang') }} :  {{ strtoupper(app()->getLocale()) }}
                            </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                @foreach (config('app.languages') as $langLocale => $langName)
                                    <a class="dropdown-item"
                                        href="{{ url()->current() }}?change_language={{ $langLocale }}">{{ strtoupper($langLocale) }}
                                        ({{ $langName }})</a>
                                @endforeach
                            </div>

                        @endif
                    </div>
                    <!-- /.col -->
                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">{{ __('text.Sign In') }}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>

        </div>
        <!-- /.card-body -->
    </div>

@endsection
