@extends('layouts.main')
@extends('layouts.sidebar')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>{{ __('text.Sell') }}</h1>
                </div>
                <div class="col-sm-8">
                    <form action="{{ url('sell/search') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="input" class="form-control form-control-lg" name="search"
                                placeholder="{{ __('text.Type Name and Price here') }}">
                            <input type="text" name="pagedd" value="5" hidden>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Sell') }} <i>({{ __('text.Contains') }} : "
                                    {{ $search }} ") </i> </h3>

                            <div class="card-tools">
                                <form>
                                    <select id="pagination">
                                        <option value="{{ url('sell/search') }}?search={{ $search }}&pagedd=5" @if ($pagedd == 5) selected @endif>5
                                        </option>
                                        <option value="{{ url('sell/search') }}?search={{ $search }}&pagedd=10" @if ($pagedd == 10) selected @endif>10
                                        </option>
                                        <option value="{{ url('sell/search') }}?search={{ $search }}&pagedd=25"
                                            @if ($pagedd == 25) selected @endif>25</option>
                                        <option value="{{ url('sell/search') }}?search={{ $search }}&pagedd=50"
                                            @if ($pagedd == 50) selected @endif>50</option>
                                    </select>
                                </form>

                            </div>
                        </div>
                        <div class="card-body">
                            <table id="t_sell" class="table display nowrap">
                                <thead>
                                    <tr>
                                    <tr>
                                        <th>{{ __('text.Item') }} </th>
                                        <th>{{ __('text.Price') }}</th>
                                        <th>{{ __('text.Discount') }}</th>
                                        <th>{{ __('text.Employee') }}</th>
                                        <th>{{ __('text.Date') }} / {{ __('text.Time') }}</th>
                                        <th style="text-align: center">{{ __('text.Action') }}</th>
                                    </tr>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sell_data as $item)
                                        <tr>
                                            <td>{{ $item->item->name }}</td>
                                            <td>{{ number_format($item->price) }}</td>
                                            <td>{{ $item->discount }}</td>
                                            <td>{{ $item->employee->first_name }}
                                                <b>{{ $item->employee->last_name }}</b></td>
                                            <td>

                                                {{ __('text.Date') }}:
                                                {{ \Carbon\Carbon::parse($item->created_at)->setTimezone(Session::get('timezone'))->format('Y-m-d') }}

                                                <br>
                                                {{ __('text.Time') }}:
                                                {{ \Carbon\Carbon::parse($item->created_at)->setTimezone(Session::get('timezone'))->format('h:i:s') }}
                                            </td>
                                            <td> - </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $sell_data->appends(['search' => $search, 'pagedd' => $pagedd])->links() }}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>


@endsection

@section('js')

    <script>
        $(function() {

            $('#pagination').on('change', function() {
                var url = $(this).val(); // get selected value
                if (url) { // require a URL
                    window.location = url; // redirect
                }
                return false;
            });
        });
    </script>

@endsection
