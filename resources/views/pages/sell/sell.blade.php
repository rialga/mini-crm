@extends('layouts.main')
@extends('layouts.sidebar')


@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>{{ __('text.Sell') }}</h1>
                </div>
                <div class="col-sm-8">
                    <form action="{{ url('sell/search') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="input" class="form-control form-control-lg" name="search"
                                placeholder="{{ __('text.Type Name and Price here') }}">
                            <input type="text" name="pagedd" value="5" hidden>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Sell') }} data</h3>
                            <div class="card-tools">
                                <button class="btn btn-block btn-success" id="btn_add" style="float:right;">
                                    {{ __('text.Add') }} {{ __('text.Sell') }}
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <table id="t_sell" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{ __('text.Item') }} </th>
                                        <th>{{ __('text.Price') }}</th>
                                        <th>{{ __('text.Discount') }}</th>
                                        <th>{{ __('text.Employee') }}</th>
                                        <th>{{ __('text.Date') }} / {{ __('text.Time') }}</th>
                                        <th style="text-align: center">{{ __('text.Action') }}</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="m_sell" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('text.Sell') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('text.Close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="f_sell" enctype="multipart/form-data">
                    <div class="modal-body">
                        {{ csrf_field() }}

                        <div class="card-body">
                            <div class="form-group">
                                <label>{{ __('text.Choose') }} {{ __('text.Item') }}</label>
                                <select required class="custom-select" id="item" name="item">
                                    <option value="">{{ __('text.Select Option') }}</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="discount">{{ __('text.Discount') }}</label>
                                <input required type="number" class="form-control" id="discount" name="discount"
                                    placeholder="{{ __('text.Enter') }} {{ __('text.Discount') }}" step="0.01" min="0" max="100">
                            </div>

                            <div class="form-group">
                                <label>{{ __('text.Choose') }} {{ __('text.Employee') }}</label>
                                <select required class="custom-select" id="employee" name="employee">
                                    <option value="">{{ __('text.Select Option') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">{{ __('text.Close') }}</button>
                        <button type="submit" class="btn btn-primary" id="btn_fsubmit">{{ __('text.Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $(document).ready(function() {
            table()
            employeeList()
            itemList()

        });


        $('#m_sell').on('hidden.bs.modal', function(e) {

            $(this).find('#f_sell')[0].reset();
            $('#btn_fsubmit').removeAttr('hidden');
            $('#discount').removeAttr('readonly');
            $('#item').removeAttr('disabled');
            $('#employee').removeAttr('disabled');
        });

        $(document).on('click', '#btn_add', function() {

            $('#btn_fsubmit').text('Submit').attr('class', 'btn btn-primary');
            $('#m_sell').modal('show');
            $('#f_sell').attr({
                action: '{{ url('sell/store') }}',
                method: 'POST'
            });

        });

        $(document).on('click', '#edit', function() {
            var data = $('#t_sell').DataTable().row($(this).parents('tr')).data();
            $('#m_sell').modal('show');
            $('#employee').val(data.employee_id).change();
            $('#discount').val(data.discount).change();
            $('#item').val(data.item_id).change();

            $('#btn_fsubmit').text("{{ __('text.Update') }}").attr({
                class: 'btn btn-warning',
                style: 'color: white;'
            });

            $('#f_sell').attr({
                action: '{{ url('sell/update') }}/' + data.id,
                method: 'POST'
            })
        });

        $(document).on('click', '#delete', function() {
            var id = $(this).data('id');
            Swal.fire({
                title: "{{ __('text.Do you want to Delete?') }}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: `Yes`,
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'delete',
                        url: "{{ url('sell/destroy') }}/" + id + '?_token=' +
                            '{{ csrf_token() }}',
                        success: function(data) {
                            if (data.status) {
                                $('#t_sell').DataTable().destroy();
                                table();
                                $('#m_sell').modal('hide');

                                alert('success', data.message)
                            } else {
                                alert('error', data.message)
                            }
                        }
                    })
                }
            });

        });

        $(document).on('click', '#show', function() {
            var data = $('#t_sell').DataTable().row($(this).parents('tr')).data();

            $('#m_sell').modal('show');
            $('#discount').val(data.discount).change().attr('readonly', 'true');
            $('#item').val(data.item_id).change().attr('disabled', 'true');
            $('#employee').val(data.employee_id).change().attr('disabled', 'true');
            $('#btn_fsubmit').attr('hidden', 'true')

        });


        $('#f_sell').submit(function(e) {
            e.preventDefault();

            let formData = new FormData(this)

            $.ajax({
                url: $(this).attr('action') + '?_token=' + '{{ csrf_token() }}',
                type: $(this).attr('method'),
                data: formData,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    proces()
                },
                success: function(data) {
                    if (data.status) {
                        $('#t_sell').DataTable().destroy();
                        table();
                        $('#m_sell').modal('hide');

                        alert('success', data.message)

                    } else {
                        alert('error', data.message)
                    }
                },
                error: function(response) {
                    const errors = response.responseJSON.errors
                    const firstErr = Object.keys(errors)[0]
                    const messageErr = errors[firstErr][0]
                    alert('error', messageErr)
                },

            });
        });

        var nf = new Intl.NumberFormat();

        const table = () => {
            $("#t_sell").DataTable({
                "searching": true,
                "paging": true,
                "searching": true,
                "chache": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,

                "ajax": "{{ url('sell/data') }}",
                "columns": [
                    {
                        data: "id",
                        render: function(data, type, row) {
                            return row.item.name
                        }
                    },
                    {
                        data: "price",
                        render: function(data) {
                            return nf.format(data)
                        }
                    },
                    {
                        data: "discount",
                        render: function(data) {
                            return data + '%'
                        }
                    },
                    {
                        data: "id",
                        "render": function(data, type, row) {
                            return row.employee.first_name + "  <b>" + row.employee.last_name + "<b>";
                        }
                    },
                    {
                        data: 'created_at',
                        render: function(data) {
                            return '<small> {{ __('text.Date') }} : ' + date(dateFull(data)) +
                                ' <br> {{ __('text.Time') }} : ' + time(dateFull(data)) + ' </small>'
                        }
                    },
                    {
                        "orderable": false,
                        data: 'id',
                        sClass: 'text-center',
                        render: function(data) {
                            return '<a href="#" data-id="' + data +
                                '" id="show" class="text-primary mr-2" title="detail"><i class="fa fa-eye"></i> </a> &nbsp;' +
                                '<a href="#" data-id="' + data +
                                '" id="edit" class="text-warning mr-2" title="edit"><i class="fa fa-edit"></i> </a> &nbsp;' +
                                '<a href="#" data-id="' + data +
                                '" id="delete" class="text-danger" title="delete"><i class="fa fa-trash-alt"></i> </a>';
                        },
                    }
                ],
            });
        };


        const itemList = () => {
            $.ajax({
                url: '{{ url('sell/item-list') }}',
                dataType: "json",
                success: function(data) {
                    var item = jQuery.parseJSON(JSON.stringify(data));
                    $.each(item, function(k, v) {
                        $('#item').append($('<option>', {
                            value: v.id
                        }).text(v.name +' ( ' +v.price+ ' )' ))
                    })
                }
            });
        }

        const employeeList = () => {
            $.ajax({
                url: '{{ url('sell/employee-list') }}',
                dataType: "json",
                success: function(data) {
                    var employee = jQuery.parseJSON(JSON.stringify(data));
                    $.each(employee, function(k, v) {
                        $('#employee').append($('<option>', {
                            value: v.id
                        }).text(v.first_name + ' ' + v.last_name))
                    })
                }
            });
        }


        const alert = (stat, message) => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                icon: stat,
                title: message
            })
        }

        const proces = () => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
            });

            Toast.fire({
                icon: 'info',
                title: 'Loading'
            })
        }

        var tzString = {!! json_encode(config('app.timezone')) !!};
        const dateFull = (data) => {
            return new Date((typeof data === "string" ? new Date(data) : data).toLocaleString("en-US", {
                timeZone: tzString
            }));
        }

        const date = (datetime) => {

            var month = '' + (datetime.getMonth() + 1)
            var day = '' + datetime.getDate()


            month.length < 2 ? month = '0' + month : month
            day.length < 2 ? day = '0' + day : day

            return datetime.getFullYear() + '-' + month + '-' + day;
        }

        const time = (datetime) => {

            var hours = '' + datetime.getHours()
            var minutes = '' + datetime.getMinutes()
            var seconds = '' + datetime.getSeconds()

            hours.length < 2 ? hours = '0' + hours : hours
            minutes.length < 2 ? minutes = '0' + minutes : minutes
            seconds.length < 2 ? seconds = '0' + seconds : seconds



            return hours + ":" + minutes + ":" + seconds;
        }
    </script>

@endsection
