@extends('layouts.main')
@extends('layouts.sidebar')


@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>{{ __('text.Employee') }} </h1>
                </div>
                <div class="col-sm-8">
                    <form action="{{ url('employee/search') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="input" class="form-control form-control-lg" name="search" placeholder="{{ __('text.Type name , email, company, datetime here')}}">
                            <input type="text" name="pagedd" value="5" hidden>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Employees data') }}</h3>
                            <div class="card-tools">
                                <button class="btn btn-block btn-success" id="btn_add" style="float:right;">
                                    {{ __('text.Add Employee') }}
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="t_employee" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        {{-- <th style="width: 10px">No</th> --}}
                                        <th>{{ __('text.Name') }}</th>
                                        <th>Email</th>
                                        <th>{{ __('text.Phone') }}</th>
                                        <th>{{ __('text.Company') }}</th>
                                        <th>{{ __('text.Join Date') }}</th>
                                        <th style="text-align: center">{{ __('text.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">

                        </div>
                        <!-- /.card-footer-->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="m_employee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('text.Employee') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="f_employee" enctype="multipart/form-data">
                    <div class="modal-body">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group">
                                <label for="firstName">{{ __('text.First Name') }}</label>
                                <input type="text" class="form-control" id="firstName" name="firstName"
                                    placeholder="{{ __('text.Enter First Name') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="lastName">{{ __('text.Last Name') }}</label>
                                <input type="text" class="form-control" id="lastName" name="lastName"
                                    placeholder="{{ __('text.Enter Last Name') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" id="email" name="email"
                                    placeholder="{{ __('text.Enter Email') }}">
                            </div>
                            <div class="form-group">
                                <label for="phone">{{ __('text.Phone') }}</label>
                                <input type="number" class="form-control" id="phone" name="phone"
                                    placeholder="{{ __('text.Enter Phone') }}">
                            </div>


                            <div class="form-group">
                                <label>{{ __('text.Choose Company') }}</label>
                                <select class="custom-select" id="company" name="company">
                                    <option value="">{{ __('text.Select Option') }}</option>
                                </select>
                            </div>

                            <div class="form-group" id="field_password">
                                <label for="password">{{ __('text.Password') }}</label>

                                <div class="input-group">
                                    <input required type="password" class="form-control" id="password" name="password"placeholder="{{ __('text.Enter Password') }}">
                                    <div class="input-group-prepend">
                                        <a id="btn_cancel" class="btn btn-danger" style="color: white;">{{ __('text.Cancel') }}</a>
                                    </div>

                                </div>
                            </div>

                            <a id="btn_change_pass" class="btn btn-warning" style="color: white;"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{ __('text.Change Password') }}</a>


                            <div class="form-group" id="field_custom" style="text-align: center;">
                                <small> {{ __('text.created by') }} : <b id="created_by"></b> </small> <br>
                                <small> {{ __('text.updated by') }} : <b id="updated_by"></b> </small>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">{{ __('text.Close') }}</button>
                            <button type="submit" id="btn_fsubmit" class="btn btn-primary">{{ __('text.Submit') }}</button>
                        </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        var tzString = {!! json_encode(config('app.timezone')) !!};
        $(document).ready(function() {
            table()
            companiesList()
        });

        $('#m_employee').on('hidden.bs.modal', function(e) {
            $(this).find('#f_employee')[0].reset();
            $('#btn_fsubmit').removeAttr('hidden');
            $('#firstName').removeAttr('readonly');
            $('#lastName').removeAttr('readonly');
            $('#phone').removeAttr('readonly');
            $('#email').removeAttr('readonly');
            $('#company').removeAttr('disabled');
            $("password").attr("required", "true");
            $('#btn_change_pass').hide()
            $('#btn_cancel').hide()

            $('#field_custom').show();


        });

        $('#f_employee').submit(function(e) {
            e.preventDefault();

            let formData = new FormData(this)

            $.ajax({
                url: $(this).attr('action') + '?_token=' + '{{ csrf_token() }}',
                type: $(this).attr('method'),
                data: formData,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data.status) {
                        $('#t_employee').DataTable().destroy();
                        table();
                        $('#m_employee').modal('hide');

                        alert('success', data.message)


                    } else {
                        alert('error', data.message)
                    }
                },
                error: function(response) {
                    const errors = response.responseJSON.errors
                    const firstErr = Object.keys(errors)[0]
                    const messageErr = errors[firstErr][0]

                    alert('error', messageErr)
                },

            });
        });

        $(document).on('click', '#btn_add', function() {
            $('#btn_fsubmit').text("{{ __('text.Submit') }}").attr('class', 'btn btn-primary');
            $('#field_custom').hide();
            $('#btn_change_pass').hide()
            $('#btn_cancel').hide()
            $('#btn_cancel').hide()
            $('#field_password').show()
            $('#password').attr('required' , 'true')



            $('#m_employee').modal('show');
            $('#f_employee').attr({
                action: '{{ url('employee/store') }}',
                method: 'POST'
            });

        });


        let val = true
        $(document).on('click', '#btn_change_pass', function() {
            if (val) {
                $('#btn_change_pass').hide();
                $('#field_password').show();
                $('#btn_cancel').show()
                $("#password").attr("required", "true");
                val = false
            }
        });

        $(document).on('click', '#btn_cancel', function() {
            if (!val) {
                $('#btn_change_pass').show()
                $('#field_password').hide()
                $('#btn_cancel').hide()
                $('#password').removeAttr('required')
                val = true
            }
        });

        $(document).on('click', '#edit', function() {
            var data = $('#t_employee').DataTable().row($(this).parents('tr')).data();

            $('#btn_change_pass').show()
            $('#password').removeAttr('required')
            $('#field_password').hide()

            $('#m_employee').modal('show');
            $('#firstName').val(data.first_name).change();
            $('#lastName').val(data.last_name).change();
            $('#phone').val(data.phone).change();
            $('#email').val(data.email).change();
            $('#company').val(data.companies_id).change();

            $('#field_custom').hide();


            $('#btn_fsubmit').text("{{ __('text.Update') }}").attr({
                class: 'btn btn-warning',
                style: 'color: white;'
            });

            $('#f_employee').attr({
                action: '{{ url('employee/update') }}/' + data.id,
                method: 'POST'
            })
        });

        $(document).on('click', '#show', function() {
            var data = $('#t_employee').DataTable().row($(this).parents('tr')).data();

            $('#m_employee').modal('show');
            $('#firstName').val(data.first_name).change().attr('readonly', 'true');
            $('#lastName').val(data.last_name).change().attr('readonly', 'true');
            $('#phone').val(data.phone).change().attr('readonly', 'true');
            $('#email').val(data.email).change().attr('readonly', 'true');
            $('#company').val(data.companies_id).change().attr('disabled', 'true');
            $('#btn_fsubmit').attr('hidden', 'true')
            $('#field_password').hide()
            $('#btn_change_pass').hide()
            $("#updated_by").text( data.updated_by == null ? '-' : data.updated_by.name );
            $("#created_by").text( data.created_by == null ? '-' : data.created_by.name );

        });



        $(document).on('click', '#delete', function() {
            var id = $(this).data('id');
            Swal.fire({
                title: "{{ __('text.Do you want to Delete?') }}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: `Yes`,
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'delete',
                        url: "{{ url('employee/destroy') }}/" + id + '?_token=' +
                            '{{ csrf_token() }}',
                        success: function(data) {
                            if (data.status) {
                                $('#t_employee').DataTable().destroy();
                                table();
                                $('#m_employee').modal('hide');

                                alert('success', data.message)
                            } else {
                                alert('error', data.message)
                            }
                        }
                    })
                }
            });

        });

        const table = () => {
            $("#t_employee").DataTable({
                "searching": true,
                "paging": true,
                "searching": true,
                "chache": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
                "scrollX": true,

                "ajax": "{{ url('employee/data') }}",
                "columns": [

                    {
                        data: "id",
                        "render": function(data, type, row) {
                            return row.first_name + "  <b>" + row.last_name + "<b>";
                        }
                    },

                    {
                        data: "email",
                        render: function(data) {
                            data == null ? data = '-' : data
                            return data
                        }
                    },
                    {
                        data: "phone",
                        render: function(data) {
                            data == null ? data = '-' : data
                            return data
                        }
                    },
                    {
                        data: "company.name",
                        render: function(data) {
                            data == null ? data = '-' : data
                            return data
                        }
                    },
                    {
                        data: 'created_at',
                        render: function(data) {
                            return '<small> {{ __('text.Date') }} : ' + date(dateFull(data)) + ' <br> {{ __('text.Time') }} : ' + time(dateFull(data)) + ' </small>'
                        }
                    },
                    {
                        "orderable": false,
                        data: 'id',
                        sClass: 'text-center',
                        render: function(data) {
                            return '<a href="#" data-id="' + data +
                                '" id="show" class="text-primary mr-2" title="detail"><i class="fa fa-eye"></i> </a> &nbsp;' +
                                '<a href="#" data-id="' + data +
                                '" id="edit" class="text-warning mr-2" title="edit"><i class="fa fa-edit"></i> </a> &nbsp;' +
                                '<a href="#" data-id="' + data +
                                '" id="delete" class="text-danger" title="delete"><i class="fa fa-trash-alt"></i> </a>';
                        },
                    }
                ],
            });
        };

        const companiesList = () => {
            $.ajax({
                url: '{{ url('employee/companyList') }}',
                dataType: "json",
                success: function(data) {
                    var company = jQuery.parseJSON(JSON.stringify(data));
                    $.each(company, function(k, v) {
                        $('#company').append($('<option>', {
                            value: v.id
                        }).text(v.name))
                    })
                }
            });
        }

        const alert = (stat, message) => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                icon: stat,
                title: message
            })
        }

        const dateFull = (data)=> {
            return new Date((typeof data === "string" ? new Date(data) : data).toLocaleString("en-US", { timeZone: tzString }));
        }

        const date =  (datetime) => {

            var month = ''+(datetime.getMonth() + 1)
            var day = ''+datetime.getDate()


            month.length < 2 ? month = '0'+ month : month
            day.length < 2 ? day = '0'+ day : day

            return datetime.getFullYear() + '-' + month + '-' + day;
        }

        const time =  (datetime) => {

            var hours = '' + datetime.getHours()
            var minutes = ''+ datetime.getMinutes()
            var seconds = ''+datetime.getSeconds()

            hours.length < 2 ? hours = '0'+ hours : hours
            minutes.length < 2 ? minutes = '0'+ minutes : minutes
            seconds.length < 2 ? seconds = '0'+ seconds : seconds



            return hours + ":" + minutes + ":" + seconds;
        }
    </script>

@endsection
