@extends('layouts.main')
@extends('layouts.sidebar')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>{{ __('text.Employee') }}</h1>
                </div>
                <div class="col-sm-8">
                    <form action="{{ url('employee/search') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="input" class="form-control form-control-lg" name="search" id="search" placeholder="Type your keywords here">
                            <input  hidden  name="pagedd" value="{{ $pagedd }}">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Companies data') }} <i>({{ __('text.Contains') }} : " {{ $search}} ") </i> </h3>

                            <div class="card-tools">
                                <form>
                                    <select id="pagination">
                                        <option value="{{ url('employee/search') }}?search={{ $search }}&pagedd=5" @if($pagedd == 5) selected @endif >5</option>
                                        <option value="{{ url('employee/search') }}?search={{ $search }}&pagedd=10" @if($pagedd == 10) selected @endif >10</option>
                                        <option value="{{ url('employee/search') }}?search={{ $search }}&pagedd=25" @if($pagedd == 25) selected @endif >25</option>
                                        <option value="{{ url('employee/search') }}?search={{ $search }}&pagedd=50" @if($pagedd == 50) selected @endif >50</option>
                                    </select>
                                </form>

                            </div>
                        </div>
                        <div class="card-body">
                            <table id="t_cemployee" class="table display nowrap">
                                <thead>
                                    <tr>
                                        {{-- <th style="width: 10px">No</th> --}}
                                        <th>{{ __('text.Name') }}</th>
                                        <th>Email</th>
                                        <th>{{ __('text.Phone') }}</th>
                                        <th>{{ __('text.Company') }}</th>
                                        <th>{{ __('text.Join Date') }}</th>
                                        <th style="text-align: center">{{ __('text.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($employee_data as $item)
                                        <tr>
                                            <td> {{ $item->first_name }} <b>{{ $item->last_name }}</b> </td>
                                            <td>{{ $item->email== null ? '-' : $item->email }}</td>
                                            <td>{{ $item->phone== null ? '-' : $item->phone }}</td>
                                            <td>{{ $item->companies_id== null ? '-' : $item->company->name }}</td>
                                            <td>
                                                {{ __('text.Date') }}:  {{ \Carbon\Carbon::parse($item->created_at )->setTimezone(Session::get('timezone'))->format('Y-m-d') }}
                                                <br>
                                                {{ __('text.Time') }}: {{ \Carbon\Carbon::parse($item->created_at )->setTimezone(Session::get('timezone'))->format('h:i:s') }}
                                            </td>
                                            <td> - </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $employee_data->appends(['search' => $search , 'pagedd' => $pagedd])->links() }}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>


@endsection

@section('js')

<script>

    $(function(){

      $('#pagination').on('change', function () {
          var url = $(this).val(); // get selected value
          if (url) { // require a URL
              window.location = url; // redirect
          }
          return false;
      });
    });

</script>

@endsection
