@extends('layouts.main')
@extends('layouts.sidebar')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>{{ __('text.Sell Summary') }} </h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">
            <div class="row">

                <div class="col-12">

                    <div class="card card-outline card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Detail</h3>

                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{ url('sell-summary/search') }}" method="GET">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                            </div>
                                            <input type="text" class="form-control float-right" name="date_range" id="date_range" value="{{ $date_range }}">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <input type="input" class="form-control form-control-lg" name="search"
                                                placeholder="Type {{ __('text.Company') }} , {{ __('text.Employee') }}">
                                            <input type="text" name="pagedd" value="5" hidden>
                                            <div class="input-group-append">
                                                <button type="submit" class="btn btn-lg btn-default">
                                                    <i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Sell Summary') }}</h3>

                            <div class="card-tools">
                                <form>
                                    <select id="pagination">
                                        <option value="{{ url('sell-summary/') }}?pagedd=5" @if ($pagedd == 5) selected @endif>5</option>
                                        <option value="{{ url('sell-summary/') }}?pagedd=10" @if ($pagedd == 10) selected @endif>10</option>
                                        <option value="{{ url('sell-summary/') }}?pagedd=25" @if ($pagedd == 25) selected @endif>25</option>
                                        <option value="{{ url('sell-summary/') }}?pagedd=50" @if ($pagedd == 50) selected @endif>50</option>
                                    </select>
                                </form>

                            </div>
                        </div>
                        <div class="card-body table-responsive p-0">
                            <table id="t_item" class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>{{ __('text.Employee') }}</th>
                                        <th>{{ __('text.Company') }}</th>
                                        <th>{{ __('text.Price') }} Total</th>
                                        <th>{{ __('text.Discount') }} Total</th>
                                        <th> Total </th>
                                        <th>{{ __('text.Date') }} / {{ __('text.Time') }}</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data as $item)
                                        <tr>
                                            <td>{{ $item->employee->first_name }} {{ $item->employee->last_name }}</td>
                                            <td>{{ $item->employee  ->company->name }}</td>
                                            <td>{{ number_format($item->price_total) }}</td>
                                            <td>{{ number_format($item->discount_total) }}</td>
                                            <td>{{ number_format($item->total) }}</td>
                                            <td>
                                                <a
                                                    href="{{ url('sell-summary/detail/'.$item->id)}}">
                                                    {{ __('text.Date') }}:
                                                    {{ \Carbon\Carbon::parse($item->created_at)->setTimezone(Session::get('timezone'))->format('Y-m-d') }}
                                                </a>
                                                <br>
                                                {{ __('text.Time') }}:
                                                {{ \Carbon\Carbon::parse($item->created_at)->setTimezone(Session::get('timezone'))->format('h:i:s') }}
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6" style="text-align: center">
                                                No Data
                                            </td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                            {{ $data->appends(['pagedd' => $pagedd])->links() }}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>


@endsection

@section('js')

<script>
    $(function() {

        $('#pagination').on('change', function() {
            var url = $(this).val(); // get selected value
            if (url) { // require a URL
                window.location = url; // redirect
            }
            return false;
        });
    });
</script>

@endsection
