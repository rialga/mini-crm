@extends('layouts.main')
@extends('layouts.sidebar')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-5">

                    @if($sell != null)
                    <h1>{{ __('text.Sell Summary') }} <small><b>
                        {{ \Carbon\Carbon::parse($sell_summary->first()->created_at)->setTimezone(Session::get('timezone'))->format('Y-m-d') }}
                     </b> </small> </h1>


                    <h3>{{ $sell->first()->employee->first_name }} {{ $sell->first()->employee->last_name }}
                        ( {{ $sell->first()->employee->company->name }} )</h3>
                    @else
                    <h3> No Data</h3>
                    @endif
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title"> {{ __('text.Sell Summary') }}</h3>
                </div>

                <div class="body">
                    <table class="table">
                        <thead>
                            <tr>

                                <td style=" text-align: left">
                                    {{ __('text.Price') }} Total <br>
                                    {{ __('text.Discount') }} Total <br>
                                    Total
                                </td>

                                <td style="text-align: left">
                                    : {{ number_format($sell_summary->price_total) }} <br>
                                    : {{ number_format($sell_summary->discount_total) }} <br>
                                    : {{ number_format($sell_summary->total) }} <br>
                                </td>

                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Sell Summary') }} Data</h3>

                            <div class="card-tools">
                                <form>
                                    <select id="pagination">
                                        <option value="{{ url('sell-summary/detail/' . $id) }}?pagedd=5"
                                            @if ($pagedd == 5) selected @endif>5
                                        </option>
                                        <option value="{{ url('sell-summary/detail/' . $id) }}?pagedd=10"
                                            @if ($pagedd == 10) selected @endif>10
                                        </option>
                                        <option value="{{ url('sell-summary/detail/' . $id) }}?pagedd=25"
                                            @if ($pagedd == 25) selected @endif>25
                                        </option>
                                        <option value="{{ url('sell-summary/detail/' . $id) }}?pagedd=50"
                                            @if ($pagedd == 50) selected @endif>50
                                        </option>
                                    </select>
                                </form>

                            </div>
                        </div>



                        <div class="card-body">
                            <table id="t_item" class="table display nowrap">
                                <thead>
                                    <tr>
                                        <th>{{ __('text.Item') }}</th>
                                        <th>{{ __('text.Price') }} </th>
                                        <th>{{ __('text.Discount') }} </th>
                                        <th>{{ __('text.Time') }}</th>

                                    </tr>
                                </thead>
                                <tbody>

                                    @forelse ( $sell as $item)
                                    <tr>
                                        <td>{{ $item->item->name }}</td>
                                        <td>{{ number_format($item->price) }}</td>
                                        <td>{{ $item->discount }}</td>
                                        <td>
                                            {{ \Carbon\Carbon::parse($item->created_at)->setTimezone(Session::get('timezone'))->format('h:i:s') }}
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="4" style="text-align: center">No Data</td>
                                    </tr>
                                    @endforelse

                                </tbody>
                            </table>
                            @if($sell != null)
                            {{ $sell->appends(['pagedd' => $pagedd])->links() }}
                            @endif
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>


@endsection

@section('js')

    <script>
        $(function() {

            $('#pagination').on('change', function() {
                var url = $(this).val(); // get selected value
                if (url) { // require a URL
                    window.location = url; // redirect
                }
                return false;
            });
        });
    </script>

@endsection
