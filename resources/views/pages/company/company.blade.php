@extends('layouts.main')
@extends('layouts.sidebar')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>{{ __('text.Company') }}</h1>
                </div>
                <div class="col-sm-8">
                    <form action="{{ url('company/search') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="input" class="form-control form-control-lg" name="search" placeholder="Type your keywords here">
                            <input type="text" name="pagedd" value="5" hidden>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Companies data') }}</h3>

                            <div class="card-tools">

                                <button class="btn btn-block btn-success" id="btn_add" style="float:right;">
                                    {{ Lang::get('text.Add Company') }}
                                </button>

                            </div>
                        </div>
                        <div class="card-body">
                            <table id="t_company" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Logo</th>
                                        <th>{{ __('text.Name') }}</th>
                                        <th>Email</th>
                                        <th>{{ __('text.Website') }}</th>
                                        <th>{{ __('text.Join Date') }}</th>
                                        <th style="text-align: center">{{ __('text.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="m_company" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('text.Company') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('text.Close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="f_company" enctype="multipart/form-data">
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">{{ __('text.Name') }}</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    placeholder="{{ __('text.Enter Name') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email"
                                    placeholder="{{ __('text.Enter Email') }}">
                            </div>
                            <div class="form-group">
                                <label for="website">{{ __('text.Website') }}</label>
                                <input type="text" class="form-control" id="website" name="website"
                                    placeholder="{{ __('text.Enter Website') }}">
                            </div>
                            <div class="form-group" id="div_inputlogo">
                                <label for="logo">Logo</label>
                                <span class="ml-2" style="color: gray; font-size:11px">*{{ __('text.Minimum Dimension') }} =
                                    100 x 100</span>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="logo" name="logo" accept="image/*">
                                        <label class="custom-file-label" for="logo">{{ __('text.Choose File') }}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="div_image" hidden>
                                <label for="image">Logo</label>
                                <div class="input-group">
                                    <img src="" width="100" height="100" id="image" />
                                </div>
                            </div>
                            <a class="btn btn-danger mt-2" id="btn_changelogo" hidden>{{ __('text.Change Logo') }}</a>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('text.Close') }}</button>
                        <button type="submit" class="btn btn-primary" id="btn_fsubmit">{{ __('text.Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="m_detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('text.Company Detail') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="card-body text-center">
                        <div class="form-group">
                            <h1 id="show_name"> Name </h1>
                        </div>

                        <div class="form-group">
                            <img id="show_logo" src="" width="100" height="100" id="image" />
                        </div>

                        <div class="form-group">
                            <h6 id="show_email" style="color:gray"> - </h6>
                            <h6 id="show_website" style="color:gray"> - </h6>


                        </div>

                        <div class="form-group mt-4">
                            <small> {{ __('text.created by') }} : <b id="created_by"></b> </small> <br>
                            <small> {{ __('text.updated by') }} : <b id="updated_by"></b> </small>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('text.Close') }}</button>
                </div>
            </div>
        </div>
    </div>


    </div>

@endsection

@section('js')

    <script>
        var tzString = {!! json_encode(config('app.timezone')) !!};
        $(document).ready(function() {
            table()
        });


        $('#m_company').on('hidden.bs.modal', function(e) {
            val = true
            document.getElementById("btn_changelogo").innerText = "{{ __('text.Change Logo') }}"

            $('#div_image').attr('hidden', 'true');
            $('#btn_changelogo').attr('hidden', 'true');
            $('#div_inputlogo').removeAttr('hidden');
            $(this).find('#f_company')[0].reset();
        });

        $('#f_company').submit(function(e) {
            e.preventDefault();

            let formData = new FormData(this)

            $.ajax({
                url: $(this).attr('action') + '?_token=' + '{{ csrf_token() }}',
                type: $(this).attr('method'),
                data: formData,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    proces()
                },
                success: function(data) {
                    if (data.status) {
                        $('#t_company').DataTable().destroy();
                        table();
                        $('#m_company').modal('hide');

                        alert('success', data.message)


                    } else {
                        alert('error', data.message)
                    }
                },
                error: function(response) {
                    const errors = response.responseJSON.errors
                    const firstErr = Object.keys(errors)[0]
                    const messageErr = errors[firstErr][0]

                    alert('error', messageErr)
                },

            });
        });


        $(document).on('click', '#btn_add', function() {

            $('#btn_fsubmit').text('Submit').attr('class', 'btn btn-primary');
            $('#m_company').modal('show');
            $('#f_company').attr({
                action: '{{ url('company/store') }}',
                method: 'POST'
            });

        });


        $(document).on('click', '#edit', function() {
            var data = $('#t_company').DataTable().row($(this).parents('tr')).data();
            $('#m_company').modal('show');
            $('#name').val(data.name).change();
            $('#email').val(data.email).change();
            $('#website').val(data.website).change();

            $('#div_inputlogo').attr('hidden', 'true');
            $('#div_image').removeAttr('hidden');
            $('#btn_changelogo').removeAttr('hidden');

            $('#btn_fsubmit').text("{{ __('text.Update') }}").attr({
                class: 'btn btn-warning',
                style: 'color: white;'
            });

            // $('#logo').val(data.tahun).change();
            if (data.logo == null) {
                $('#image').prop("src", "storage/uploads/no-data.png").change();
            } else {
                $('#image').prop("src", "storage/uploads/" + data.logo).change();

            }

            $('#f_company').attr({
                action: '{{ url('company/update') }}/' + data.id,
                method: 'POST'
            })
        });


        $(document).on('click', '#show', function() {
            var data = $('#t_company').DataTable().row($(this).parents('tr')).data();

            data.logo == null ? data.logo = 'no-data.png' : data.logo
            $('#show_logo').prop("src", "storage/uploads/" + data.logo).change();
            $('#show_name').text(data.name).change();
            $('#show_email').text(data.email).change();
            $('#show_website').text(data.website).change();

            $("#updated_by").text( data.updated_by == null ? '-' : data.updated_by.name );
            $("#created_by").text( data.created_by == null ? '-' : data.created_by.name );


            $('#m_detail').modal('show');

        });

        let val = true
        $(document).on('click', '#btn_changelogo', function() {

            if (val) {
                $('#div_image').attr('hidden', 'true');
                $('#div_inputlogo').removeAttr('hidden');
                document.getElementById("btn_changelogo").innerText = "{{ __('text.Cancel') }}"
                val = false
            } else {
                $('#div_image').removeAttr('hidden');
                $('#div_inputlogo').attr('hidden', 'true');
                document.getElementById("btn_changelogo").innerText = "Change Logo"
                val = true
            }
        });


        $(document).on('click', '#delete', function() {
            var id = $(this).data('id');
            Swal.fire({
                title: "{{ __('text.Do you want to Delete?') }}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: `Yes`,
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'delete',
                        url: "{{ url('company/destroy') }}/" + id + '?_token=' +
                            '{{ csrf_token() }}',
                        success: function(data) {
                            if (data.status) {
                                $('#t_company').DataTable().destroy();
                                table();
                                $('#m_company').modal('hide');

                                alert('success', data.message)
                            } else {
                                alert('error', data.message)
                            }
                        }
                    })
                }
            });

        });

        const table = () => {
            $("#t_company").DataTable({
                "searching": true,
                "paging": true,
                "searching": true,
                "chache": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,

                "ajax": "{{ url('company/data') }}",
                "columns": [{
                    "orderable": false,
                        data: "logo",
                        render: function(data) {
                            data == null ? data = 'no-data.png' : data
                            return '<img src="storage/uploads/' + data +
                                '" border="0" width="40"  align="center" />';
                        }
                    },
                    {
                        "data": "name"
                    },
                    {
                        data: "email",
                        render: function(data) {
                            data == null ? data = '-' : data
                            return data
                        }
                    },
                    {
                        data: "website",
                        render: function(data) {
                            data == null ? data = '-' : data
                            return data
                        }
                    },
                    {
                        data: 'created_at',
                        render: function(data) {
                            return '<small> {{ __('text.Date') }} : '+date(dateFull(data))+' <br> {{ __('text.Time') }} : '+time(dateFull(data))+' </small>'
                        }
                    },
                    {
                        "orderable": false,
                        data: 'id',
                        sClass: 'text-center',
                        render: function(data) {
                            return '<a href="#" data-id="' + data +
                                '" id="show" class="text-primary mr-2" title="detail"><i class="fa fa-eye"></i> </a> &nbsp;' +
                                '<a href="#" data-id="' + data +
                                '" id="edit" class="text-warning mr-2" title="edit"><i class="fa fa-edit"></i> </a> &nbsp;' +
                                '<a href="#" data-id="' + data +
                                '" id="delete" class="text-danger" title="delete"><i class="fa fa-trash-alt"></i> </a>';
                        },
                    }
                ],
            });
        };


        const alert = (stat, message) => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                icon: stat,
                title: message
            })
        }

        const proces = () => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
            });

            Toast.fire({
                icon: 'info',
                title: 'Loading'
            })
        }


        const dateFull = (data)=> {
            return new Date((typeof data === "string" ? new Date(data) : data).toLocaleString("en-US", { timeZone: tzString }));
        }

        const date =  (datetime) => {
            var month = ''+(datetime.getMonth() + 1)
            var day = ''+datetime.getDate()


            month.length < 2 ? month = '0'+ month : month
            day.length < 2 ? day = '0'+ day : day

            return datetime.getFullYear() + '-' + month + '-' + day;
        }

        const time =  (datetime) => {

            var hours = '' + datetime.getHours()
            var minutes = ''+ datetime.getMinutes()
            var seconds = ''+datetime.getSeconds()

            hours.length < 2 ? hours = '0'+ hours : hours
            minutes.length < 2 ? minutes = '0'+ minutes : minutes
            seconds.length < 2 ? seconds = '0'+ seconds : seconds

            return hours + ":" + minutes + ":" + seconds;
        }
    </script>


@endsection
