@extends('layouts.main')
@extends('layouts.sidebar')

@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>{{ __('text.Item') }}</h1>
                </div>
                <div class="col-sm-8">
                    <form action="{{ url('item/search') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="input" class="form-control form-control-lg" name="search"
                                placeholder="{{ __('text.Type Name and Price here') }}">
                            <input type="text" name="pagedd" value="5" hidden>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Item') }} <i>({{ __('text.Contains') }} : "
                                    {{ $search }} ") </i> </h3>

                            <div class="card-tools">
                                <form>
                                    <select id="pagination">
                                        <option value="{{ url('item/search') }}?search={{ $search }}&pagedd=5" @if ($pagedd == 5) selected @endif>5
                                        </option>
                                        <option value="{{ url('item/search') }}?search={{ $search }}&pagedd=10" @if ($pagedd == 10) selected @endif>10
                                        </option>
                                        <option value="{{ url('item/search') }}?search={{ $search }}&pagedd=25"
                                            @if ($pagedd == 25) selected @endif>25</option>
                                        <option value="{{ url('item/search') }}?search={{ $search }}&pagedd=50"
                                            @if ($pagedd == 50) selected @endif>50</option>
                                    </select>
                                </form>

                            </div>
                        </div>
                        <div class="card-body">
                            <table id="t_item" class="table display nowrap">
                                <thead>
                                    <tr>
                                        <th>{{ __('text.Name') }}</th>
                                        <th>{{ __('text.Price') }}</th>
                                        <th style="text-align: center">{{ __('text.Action') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($item_data as $item)
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ number_format($item->price) }}</td>
                                            <td style="text-align: center">
                                                <a href="#" onclick="clickShow({{ $item->id }})"
                                                    class="text-primary mr-2" title="detail"><i class="fa fa-eye"></i> </a>
                                                &nbsp;
                                                <a href="#" onclick="clickEdit({{ $item->id }})"
                                                    class="text-warning mr-2" title="detail"><i class="fa fa-edit"></i> </a>
                                                &nbsp;
                                                <a href="#" onclick="clickDelete({{ $item->id }})"
                                                    class="text-danger mr-2" title="detail"><i class="fa fa-trash-alt"> </i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {{ $item_data->appends(['search' => $search, 'pagedd' => $pagedd])->links() }}
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="m_item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('text.Item') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('text.Close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="f_item" enctype="multipart/form-data">
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">{{ __('text.Name') }}</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    placeholder="{{ __('text.Enter Name') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="price">{{ __('text.Price') }}</label>
                                <input type="number" class="form-control" id="price" name="price"
                                    placeholder="{{ __('text.Enter') }} {{ __('text.Price') }}">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"
                            data-dismiss="modal">{{ __('text.Close') }}</button>
                        <button type="submit" class="btn btn-primary" id="btn_fsubmit">{{ __('text.Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $(function() {

            $('#pagination').on('change', function() {
                var url = $(this).val(); // get selected value
                if (url) { // require a URL
                    window.location = url; // redirect
                }
                return false;
            });
        });

        $('#m_item').on('hidden.bs.modal', function(e) {

            $(this).find('#f_item')[0].reset();
            $('#btn_fsubmit').removeAttr('hidden');
            $('#name').removeAttr('readonly');
            $('#price').removeAttr('readonly');
        });


        function clickShow(id) {

            $('#name').attr('readonly', 'true');
            $('#price').attr('readonly', 'true');
            $('#btn_fsubmit').attr('hidden', 'true')
            ajax(id)
            $('#m_item').modal('show');

        }


        function clickEdit(id) {
            ajax(id)

            $('#btn_fsubmit').text("{{ __('text.Update') }}").attr({
                class: 'btn btn-warning',
                style: 'color: white;'
            });


            $('#f_item').attr({
                action: '{{ url('item/update') }}/' + id,
                method: 'POST'
            })

            $('#m_item').modal('show');
        }


        function clickDelete(id) {
            Swal.fire({
                title: "{{ __('text.Do you want to Delete?') }}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: `Yes`,
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'delete',
                        url: "{{ url('item/destroy') }}/" + id + '?_token=' +
                            '{{ csrf_token() }}',
                        success: function(data) {
                            alert('success', data.message)
                            location.reload();
                        }
                    })
                }
            });
        }



        $('#f_item').submit(function(e) {
            e.preventDefault();

            let formData = new FormData(this)

            $.ajax({
                url: $(this).attr('action') + '?_token=' + '{{ csrf_token() }}',
                type: $(this).attr('method'),
                data: formData,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    proces()
                },
                success: function(data) {

                    alert('success', data.message)
                    location.reload();
                },
                error: function(response) {
                    const errors = response.responseJSON.errors
                    const firstErr = Object.keys(errors)[0]
                    const messageErr = errors[firstErr][0]
                    alert('error', messageErr)
                },

            });
        });

        function ajax(id) {
            $.ajax({
                url: "{{ url('item/ajax') }}/" + id + '?_token=' + '{{ csrf_token() }}',
                type: "GET",
                success: function(response) {
                    $('#name').val(response.name).change();
                    $('#price').val(response.price).change();
                }
            });
        }


        const alert = (stat, message) => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                icon: stat,
                title: message
            })
        }

        const proces = () => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
            });

            Toast.fire({
                icon: 'info',
                title: 'Loading'
            })
        }
    </script>

@endsection
