@extends('layouts.main')
@extends('layouts.sidebar')


@section('content')

    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-4">
                    <h1>{{ __('text.Item') }}</h1>
                </div>
                <div class="col-sm-8">
                    <form action="{{ url('item/search') }}" method="GET">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input type="input" class="form-control form-control-lg" name="search" placeholder="{{ __('text.Type Name and Price here')}}">
                            <input type="text" name="pagedd" value="5" hidden>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-lg btn-default">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>


    <section class="content">

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- Default box -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">{{ __('text.Item') }} data</h3>
                            <div class="card-tools">
                                <button class="btn btn-block btn-success" id="btn_add" style="float:right;">
                                    {{ __('text.Add') }} {{ __('text.Item') }}
                                </button>
                            </div>
                        </div>

                        <div class="card-body">
                            <table id="t_item" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>{{ __('text.Name') }}</th>
                                        <th>{{ __('text.Price') }}</th>
                                        <th style="text-align: center">{{ __('text.Action') }}</th>
                                    </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="m_item" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{ __('text.Item') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('text.Close') }}">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="f_item" enctype="multipart/form-data">
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">{{ __('text.Name') }}</label>
                                <input type="text" class="form-control" id="name" name="name"
                                    placeholder="{{ __('text.Enter Name') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="price">{{ __('text.Price') }}</label>
                                <input type="number" class="form-control" id="price" name="price"
                                    placeholder="{{ __('text.Enter') }} {{ __('text.Price') }}">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('text.Close') }}</button>
                        <button type="submit" class="btn btn-primary" id="btn_fsubmit">{{ __('text.Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')

    <script>
        $(document).ready(function() {
            table()
        });


        $('#m_item').on('hidden.bs.modal', function(e) {

            $(this).find('#f_item')[0].reset();
            $('#btn_fsubmit').removeAttr('hidden');
            $('#name').removeAttr('readonly');
            $('#price').removeAttr('readonly');
        });

        $(document).on('click', '#btn_add', function() {

            $('#btn_fsubmit').text('Submit').attr('class', 'btn btn-primary');
            $('#m_item').modal('show');
            $('#f_item').attr({
                action: '{{ url('item/store') }}',
                method: 'POST'
            });

        });

        $(document).on('click', '#edit', function() {
            var data = $('#t_item').DataTable().row($(this).parents('tr')).data();
            $('#m_item').modal('show');
            $('#name').val(data.name).change();
            $('#price').val(data.price).change();

            $('#f_item').attr({
                action: '{{ url('item/update') }}/' + data.id,
                method: 'POST'
            })
        });

        $(document).on('click', '#delete', function() {
            var id = $(this).data('id');
            Swal.fire({
                title: "{{ __('text.Do you want to Delete?') }}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: `Yes`,
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: 'delete',
                        url: "{{ url('item/destroy') }}/" + id + '?_token=' +
                            '{{ csrf_token() }}',
                        success: function(data) {
                            if (data.status) {
                                $('#t_item').DataTable().destroy();
                                table();
                                $('#m_item').modal('hide');

                                alert('success', data.message)
                            } else {
                                alert('error', data.message)
                            }
                        }
                    })
                }
            });

        });

        $(document).on('click', '#show', function() {
            var data = $('#t_item').DataTable().row($(this).parents('tr')).data();

            $('#m_item').modal('show');
            $('#name').val(data.name).change().attr('readonly', 'true');
            $('#price').val(data.price).change().attr('readonly', 'true');
            $('#btn_fsubmit').attr('hidden', 'true')

        });


        $('#f_item').submit(function(e) {
            e.preventDefault();

            let formData = new FormData(this)

            $.ajax({
                url: $(this).attr('action') + '?_token=' + '{{ csrf_token() }}',
                type: $(this).attr('method'),
                data: formData,
                dataType: 'JSON',
                processData: false,
                contentType: false,
                beforeSend: function() {
                    proces()
                },
                success: function(data) {
                    if (data.status) {
                        $('#t_item').DataTable().destroy();
                        table();
                        $('#m_item').modal('hide');

                        alert('success', data.message)

                    } else {
                        alert('error', data.message)
                    }
                },
                error: function(response) {
                    const errors = response.responseJSON.errors
                    const firstErr = Object.keys(errors)[0]
                    const messageErr = errors[firstErr][0]
                    alert('error', messageErr)
                },

            });
        });

        var nf = new Intl.NumberFormat();
        const table = () => {
            $("#t_item").DataTable({
                "searching": true,
                "paging": true,
                "searching": true,
                "chache": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,

                "ajax": "{{ url('item/data') }}",
                "columns": [{
                        "data": "name"
                    },
                    {
                        data: "price",
                        render: function(data) {
                            return nf.format(data)
                        }
                    },
                    {
                        "orderable": false,
                        data: 'id',
                        sClass: 'text-center',
                        render: function(data) {
                            return '<a href="#" data-id="' + data +
                                '" id="show" class="text-primary mr-2" title="detail"><i class="fa fa-eye"></i> </a> &nbsp;' +
                                '<a href="#" data-id="' + data +
                                '" id="edit" class="text-warning mr-2" title="edit"><i class="fa fa-edit"></i> </a> &nbsp;' +
                                '<a href="#" data-id="' + data +
                                '" id="delete" class="text-danger" title="delete"><i class="fa fa-trash-alt"></i> </a>';
                        },
                    }
                ],
            });
        };

        const alert = (stat, message) => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });

            Toast.fire({
                icon: stat,
                title: message
            })
        }

        const proces = () => {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
            });

            Toast.fire({
                icon: 'info',
                title: 'Loading'
            })
        }
    </script>

@endsection
